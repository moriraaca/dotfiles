inoremap jk <ESC>
filetype plugin indent on
syntax on
set encoding=utf-8
set nu!
set hlsearch

set ts=2
set sts=2
set sw=2
set et

set backspace=2
set backspace=indent,eol,start

set nocompatible

"let g:netrw_banner = 0
"let g:netrw_liststyle = 3
"let g:netrw_browse_split = 4
"let g:netrw_altv = 1
"let g:netrw_winsize = 25
"augroup ProjectDrawer
"  autocmd!
"  autocmd VimEnter * :Vexplore
"augroup END

autocmd vimenter * NERDTree
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

if has("gui_running")
  if has("gui_win32")
    "set guifont=Consolas:h11:cANSI
    set guifont=Courier\ New:h11:cANSI
  endif
endif